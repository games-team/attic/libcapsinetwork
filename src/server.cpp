// Copyright (c) 2004 Rob Kaper <rob@unixcode.org>. All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
//
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY AUTHOR AND CONTRIBUTORS `AS IS'' AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED.  IN NO EVENT SHALL AUTHOR OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
// OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
// OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
// SUCH DAMAGE.

// Example server demonstrating libcapsi_network.

#include <string>
#include <stdio.h>

// In real applications, use the following include instead of the local.
// #include <libcapsinetwork/socket.h>
#include "socket.h"

#include "server.h"

Server::Server() : Listener()
{
	if ( addListenPort( 1776 ) == -1 )
	{
		printf("Could not bind listenPort\n");
		exit(1);
	}
}

void Server::socketHandler( Socket *socket, const std::string &data )
{
	switch( socket->status() )
	{
		case Socket::New:
			socket->ioWrite( std::string( "you are a new connection. welcome.\n" ) );
			break;
			
		case Socket::Close:
		case Socket::Closed:
			break;
			
		case Socket::Ok:
			printf( "[%03d] %s\n", socket->fd(), data.c_str() );
			socket->ioWrite( std::string( "your input is appreciated.\n" ) );
			break;
	}
}

int main(int argc, char **argv)
{
	Server *server = new Server();

	for(;;)
		server->checkActivity();

	delete server;

	return 0;
}
